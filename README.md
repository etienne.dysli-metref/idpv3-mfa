Multifactor authentication extension for the IdP version 3
==========================================================

Read-only Git access: `https://gitlab.switch.ch/etienne.dysli-metref/idpv3-mfa.git`

Installation and testing instructions
=====================================

In order to try this project on your own IdP, follow the instructions
below. They assume your IdP was installed according to
[SWITCH's installation guide][idpguide].

0. Compile the code
-------------------

From the root of this project, run:
`mvn clean verify`

Requirements:
- Java JDK version 7 or 8
- Maven 3.3

1. New files
------------

From the source of this project, copy:
- `mfa-flows/src/main/resources/flows/authn/radius/radius-beans.xml`
- `mfa-flows/src/main/resources/flows/authn/radius/radius-flow.xml`
- `mfa-flows/src/main/resources/views/otp-login.vm`
- `mfa-flows/target/mfa-flows-1.0.0.jar`
- `tinyradius/target/tinyradius-1.1.0.jar`

to your IdP, respectively:
- `${IDP_HOME}/flows/authn/radius/radius-beans.xml`
- `${IDP_HOME}/flows/authn/radius/radius-flow.xml`
- `${IDP_HOME}/views/otp-login.vm`
- `${IDP_HOME}/edit-webapp/WEB-INF/lib/mfa-flows-1.0.0.jar`
- `${IDP_HOME}/edit-webapp/WEB-INF/lib/tinyradius-1.1.0.jar`

Be sure to delete old library versions in
`${IDP_HOME}/edit-webapp/WEB-INF/lib` and
`${IDP_HOME}/webapp/WEB-INF/lib`:

    rm ${IDP_HOME}/edit-webapp/WEB-INF/lib/mfa-flows-*-SNAPSHOT.jar \
       ${IDP_HOME}/webapp/WEB-INF/lib/mfa-flows-*-SNAPSHOT.jar \
       ${IDP_HOME}/edit-webapp/WEB-INF/lib/tinyradius-*-SNAPSHOT.jar \
       ${IDP_HOME}/webapp/WEB-INF/lib/tinyradius-*-SNAPSHOT.jar

2. Modified files
-----------------

2.1 In `conf/authn/general-authn.xml`, add a new bean for the "radius"
flow, attaching it to the authentication context class reference
`http://id.incommon.org/assurance/mfa`:

    <bean id="authn/radius" parent="shibboleth.AuthenticationFlow"
          p:forcedAuthenticationSupported="true"
          p:nonBrowserSupported="false">
      <property name="supportedPrincipals">
        <list>
          <bean parent="shibboleth.SAML2AuthnContextClassRef"
                c:classRef="http://id.incommon.org/assurance/mfa" />
        </list>
      </property>
    </bean>

2.2 In `conf/idp.properties`, set the "Password" flow as initial flow
and add the "radius" flow to the list of enabled flows:

    idp.authn.flows.initial = Password
    idp.authn.flows = Password|radius

Also in `conf/idp.properties`, configure the RADIUS server's host name
and shared secret for OTP verification:

    idp.radius.host = radius.example.org
    idp.radius.secret = secret

2.3 Add a new message in `messages/authn-messages.properties` for the
"sms_otp_sent" event and the "request SMS OTP" button:

    idp.login.sms_otp_sent = SMS OTP sent
    idp.login.requestsms = Request SMS OTP

3. Rebuild and restart IdP
--------------------------

3.1 Rebuild the IdP WAR
`JAVACMD=/usr/bin/java /opt/shibboleth-idp/bin/build.sh -Didp.target.dir=/opt/shibboleth-idp`

3.2 Restart your IdP to load the new configuration.

4. Testing
----------

Use a login link with the `authnContextClassRef` parameter set to
`http://id.incommon.org/assurance/mfa` to trigger the execution
of the "radius" flow. For example:
https://attribute-viewer.aai.switch.ch/Shibboleth.sso/Login?entityID=https://mfa-dev.ed.switch.ch/idp/shibboleth&target=https://attribute-viewer.aai.switch.ch/aai/&authnContextClassRef=http://id.incommon.org/assurance/mfa

[idpguide]: https://www.switch.ch/aai/guides/idp/installation/ "Shibboleth Identity Provider (IdP) 3 Installation Guide"
