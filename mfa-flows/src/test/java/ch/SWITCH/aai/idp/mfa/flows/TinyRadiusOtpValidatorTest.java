package ch.SWITCH.aai.idp.mfa.flows;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;
import static org.mockito.Matchers.anyString;

import java.io.IOException;

import org.junit.Test;
import org.mockito.Mockito;
import org.tinyradius.util.RadiusClient;
import org.tinyradius.util.RadiusException;

public class TinyRadiusOtpValidatorTest {

  private final RadiusClient mockRadiusClient = Mockito.mock(RadiusClient.class);
  private final OtpValidator ov = new TinyRadiusOtpValidator(mockRadiusClient);

  @Test
  public void implementsOtpValidator() {
    assertThat(ov, isA(OtpValidator.class));
  }

  @Test
  public void validateShouldPropagateCallWithSameArguments() throws IOException, RadiusException {
    ov.validate("testuser", "testpw");
    Mockito.verify(mockRadiusClient).authenticate("testuser", "testpw");
  }

  @Test
  public void validateShouldReturnOutputFromAuthenticateTrue() throws IOException, RadiusException {
    Mockito.when(mockRadiusClient.authenticate(anyString(), anyString())).thenReturn(true);
    assertThat(ov.validate("testuser", "testpw"), is(true));
  }

  @Test
  public void validateShouldReturnOutputFromAuthenticateFalse() throws IOException, RadiusException {
    Mockito.when(mockRadiusClient.authenticate(anyString(), anyString())).thenReturn(false);
    assertThat(ov.validate("testuser", "testpw"), is(false));
  }

  @Test
  public void validate_shouldWrapIOExceptionInRuntimeException() throws IOException, RadiusException {
    IOException ioe = new IOException();
    Mockito.when(mockRadiusClient.authenticate(anyString(), anyString())).thenThrow(ioe);
    try {
      ov.validate("testuser", "testpw");
    } catch (RuntimeException re) {
      assertThat((IOException) re.getCause(), is(sameInstance(ioe)));
    }
  }

  @Test
  public void validate_shouldWrapRadiusExceptionInRuntimeException() throws IOException, RadiusException {
    RadiusException rade = new RadiusException(null);
    Mockito.when(mockRadiusClient.authenticate(anyString(), anyString())).thenThrow(rade);
    try {
      ov.validate("testuser", "testpw");
    } catch (RuntimeException re) {
      assertThat((RadiusException) re.getCause(), is(sameInstance(rade)));
    }
  }

  @Test
  public void requestSmsOtp_shouldPropagateCallWithSameUsername() throws IOException, RadiusException {
    ov.requestSmsOtp("testuser");
    Mockito.verify(mockRadiusClient).authenticate("testuser", "sms");
  }

  @Test
  public void requestSmsOtp_shouldWrapIOExceptionInRuntimeException() throws IOException, RadiusException {
    IOException ioe = new IOException();
    Mockito.when(mockRadiusClient.authenticate(anyString(), anyString())).thenThrow(ioe);
    try {
      ov.requestSmsOtp("testuser");
    } catch (RuntimeException re) {
      assertThat((IOException) re.getCause(), is(sameInstance(ioe)));
    }
  }

  @Test
  public void requestSmsOtp_shouldWrapRadiusExceptionInRuntimeException() throws IOException, RadiusException {
    RadiusException rade = new RadiusException(null);
    Mockito.when(mockRadiusClient.authenticate(anyString(), anyString())).thenThrow(rade);
    try {
      ov.requestSmsOtp("testuser");
    } catch (RuntimeException re) {
      assertThat((RadiusException) re.getCause(), is(sameInstance(rade)));
    }
  }
}
