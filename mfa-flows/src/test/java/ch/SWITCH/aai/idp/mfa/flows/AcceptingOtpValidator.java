package ch.SWITCH.aai.idp.mfa.flows;

/**
 * OtpValidator implementation that always accepts any username + OTP pair.
 */
public class AcceptingOtpValidator implements OtpValidator {

  @Override
  public boolean validate(String username, String otp) {
    return true;
  }

  @Override
  public void requestSmsOtp(String username) {}
}
