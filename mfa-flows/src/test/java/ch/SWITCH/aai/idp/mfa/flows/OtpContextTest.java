package ch.SWITCH.aai.idp.mfa.flows;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import org.junit.Test;
import org.opensaml.messaging.context.BaseContext;

public class OtpContextTest {

  private final OtpContext oc = new OtpContext();

  @Test
  public void extendsBaseContext() {
    assertThat(oc, isA(BaseContext.class));
  }

  @Test
  public void canSetOtp() {
    oc.setOtp("test");
    assertThat(oc.getOtp(), is("test"));
  }

  @Test
  public void setOtpShouldReturnThis() {
    assertThat(oc.setOtp("test"),is(sameInstance(oc)));
  }
}
