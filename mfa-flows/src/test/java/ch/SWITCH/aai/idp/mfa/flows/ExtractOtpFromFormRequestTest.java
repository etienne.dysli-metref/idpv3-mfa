package ch.SWITCH.aai.idp.mfa.flows;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import net.shibboleth.idp.authn.AbstractExtractionAction;
import net.shibboleth.idp.authn.context.AuthenticationContext;
import net.shibboleth.utilities.java.support.component.ComponentInitializationException;
import net.shibboleth.utilities.java.support.component.UnmodifiableComponentException;
import net.shibboleth.utilities.java.support.logic.ConstraintViolationException;

import org.junit.Test;
import org.opensaml.profile.context.EventContext;
import org.opensaml.profile.context.ProfileRequestContext;
import org.springframework.mock.web.MockHttpServletRequest;

public class ExtractOtpFromFormRequestTest {
  private final ExtractOtpFromFormRequest<Object, Object> eoffr = new ExtractOtpFromFormRequest<Object, Object>();

  @Test
  public void mustExtendAbstractExtractionAction() {
    assertThat(eoffr, isA(AbstractExtractionAction.class));
  }

  @Test
  public void canSetFieldName() {
    eoffr.setOtpFieldName("test");
    assertThat(eoffr.getOtpFieldName(), is("test"));
  }

  @Test(expected = ConstraintViolationException.class)
  public void setFieldNameShouldRejectNull() {
    eoffr.setOtpFieldName(null);
  }

  @Test(expected = ConstraintViolationException.class)
  public void setFieldNameShouldRejectEmpty() {
    eoffr.setOtpFieldName("");
  }

  @Test(expected = UnmodifiableComponentException.class)
  public void cannotSetFieldNameAfterInitialization() throws ComponentInitializationException {
    eoffr.initialize();
    eoffr.setOtpFieldName("test");
  }

  @Test
  public void doExecute_shouldCreateNewAuthenticationSubcontext() {
    AuthenticationContext ac = new AuthenticationContext();
    eoffr.setHttpServletRequest(new MockHttpServletRequest());
    eoffr.doExecute(new ProfileRequestContext<Object, Object>(), ac);
    assertThat(ac.getSubcontext(OtpContext.class), is(notNullValue(OtpContext.class)));
  }
  
  @Test
  public void doExecute_shouldInitialiseOtpInSubcontextToNull() {
    AuthenticationContext ac = new AuthenticationContext();
    OtpContext oc = new OtpContext();
    oc.setOtp("not null");
    ac.addSubcontext(oc, true);
    eoffr.setHttpServletRequest(new MockHttpServletRequest());
    eoffr.doExecute(new ProfileRequestContext<Object, Object>(), ac);
    assertThat(ac.getSubcontext(OtpContext.class).getOtp(), is(nullValue()));
  }

  @Test
  public void doExecute_shouldSetOtpInSubcontextFromHttpRequest() {
    AuthenticationContext ac = new AuthenticationContext();
    OtpContext oc = new OtpContext();
    ac.addSubcontext(oc, true);
    MockHttpServletRequest request = new MockHttpServletRequest();
    request.setParameter(eoffr.getOtpFieldName(), "testotp");
    eoffr.setHttpServletRequest(request);
    eoffr.doExecute(new ProfileRequestContext<Object, Object>(), ac);
    assertThat(oc.getOtp(), is("testotp"));
  }

  @Test
  public void doExecute_whenHttpRequestIsNull_shouldSetNoCredentialsEvent() {
    ProfileRequestContext<Object, Object> prc = new ProfileRequestContext<Object, Object>();
    eoffr.setHttpServletRequest(null);
    eoffr.doExecute(prc, new AuthenticationContext());
    assertThat(prc.getSubcontext(EventContext.class).getEvent(), is(instanceOf(String.class)));
    assertThat((String) prc.getSubcontext(EventContext.class).getEvent(), is("NoCredentials"));
  }

  @Test
  public void doExecute_whenOtpParameterIsNull_shouldSetNoCredentialsEvent() {
    ProfileRequestContext<Object, Object> prc = new ProfileRequestContext<Object, Object>();
    MockHttpServletRequest request = new MockHttpServletRequest();
    request.removeParameter(eoffr.getOtpFieldName());
    eoffr.setHttpServletRequest(request);
    eoffr.doExecute(prc, new AuthenticationContext());
    assertThat(prc.getSubcontext(EventContext.class).getEvent(), is(instanceOf(String.class)));
    assertThat((String) prc.getSubcontext(EventContext.class).getEvent(), is("NoCredentials"));
  }

  @Test
  public void doExecute_whenOtpParameterIsEmpty_shouldSetNoCredentialsEvent() {
    ProfileRequestContext<Object, Object> prc = new ProfileRequestContext<Object, Object>();
    MockHttpServletRequest request = new MockHttpServletRequest();
    request.setParameter(eoffr.getOtpFieldName(), "");
    eoffr.setHttpServletRequest(request);
    eoffr.doExecute(prc, new AuthenticationContext());
    assertThat(prc.getSubcontext(EventContext.class).getEvent(), is(instanceOf(String.class)));
    assertThat((String) prc.getSubcontext(EventContext.class).getEvent(), is("NoCredentials"));
  }
}
