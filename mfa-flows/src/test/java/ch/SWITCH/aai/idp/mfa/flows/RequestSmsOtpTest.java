package ch.SWITCH.aai.idp.mfa.flows;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.eq;

import java.util.HashSet;
import java.util.Set;

import javax.security.auth.Subject;

import net.shibboleth.idp.authn.AbstractExtractionAction;
import net.shibboleth.idp.authn.AuthenticationFlowDescriptor;
import net.shibboleth.idp.authn.AuthenticationResult;
import net.shibboleth.idp.authn.context.AuthenticationContext;
import net.shibboleth.idp.authn.principal.UsernamePrincipal;
import net.shibboleth.utilities.java.support.logic.ConstraintViolationException;

import org.junit.Test;
import org.mockito.Mockito;
import org.opensaml.profile.context.EventContext;
import org.opensaml.profile.context.ProfileRequestContext;

public class RequestSmsOtpTest {

  private final OtpValidator mockOtpValidator = Mockito.mock(OtpValidator.class);
  private final RequestSmsOtp<Object, Object> rso = new RequestSmsOtp<Object, Object>(mockOtpValidator);

  @Test
  public void shouldExtendAbstractExtractionAction() {
    assertThat(rso, isA(AbstractExtractionAction.class));
  }

  @Test(expected = ConstraintViolationException.class)
  public void shouldThrowWhenGivenNullOtpValidator() {
    new RequestSmsOtp<Object, Object>(null);
  }

  @Test
  public void doExecute_shouldCallRequestSmsOtpWithUsername() {
    rso.doExecute(new ProfileRequestContext<Object, Object>(), mockAuthenticationContext());
    Mockito.verify(mockOtpValidator, Mockito.times(1)).requestSmsOtp(eq("testuser"));
  }

  @Test
  public void doExecute_shouldCreateEventInContext() {
    ProfileRequestContext<Object, Object> prc = new ProfileRequestContext<Object, Object>();
    rso.doExecute(prc, mockAuthenticationContext());
    EventContext<String> ec = prc.getSubcontext(EventContext.class);
    assertThat(ec, is(notNullValue()));
    assertThat(ec.getEvent(), is(equalTo("sms_otp_sent")));
  }

  @Test
  public void extractActiveSubject_shouldReturnInitialAuthnSubject() {
    Subject s = new Subject();
    assertThat(rso.extractActiveSubject(mockAuthenticationContext(s)), is(sameInstance(s)));
  }

  @Test
  public void extractActiveSubject_whenInitialAuthenticationResultIsNull_shouldReturnActiveSubject() {
    Subject s = new Subject();
    AuthenticationContext ac = mockAuthenticationContext(s);
    ac.setInitialAuthenticationResult(null);
    assertThat(rso.extractActiveSubject(ac), is(sameInstance(s)));
  }

  private AuthenticationContext mockAuthenticationContext() {
    return mockAuthenticationContext(mockSubjectWithUsernamePrincipal("testuser"));
  }

  private AuthenticationContext mockAuthenticationContext(Subject initialAuthenticationSubject) {
    AuthenticationContext ac = new AuthenticationContext();
    AuthenticationResult ar = new AuthenticationResult("authn/Password", initialAuthenticationSubject);
    Set<AuthenticationResult> activeResults = new HashSet<AuthenticationResult>();
    activeResults.add(ar);
    ac.setActiveResults(activeResults);
    ac.setInitialAuthenticationResult(ar);
    ac.setAttemptedFlow(new AuthenticationFlowDescriptor());
    return ac;
  }

  private Subject mockSubjectWithUsernamePrincipal(String username) {
    Subject s = new Subject();
    s.getPrincipals().add(new UsernamePrincipal(username));
    return s;
  }
}
