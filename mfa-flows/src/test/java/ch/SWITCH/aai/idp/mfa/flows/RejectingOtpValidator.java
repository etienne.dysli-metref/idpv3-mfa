package ch.SWITCH.aai.idp.mfa.flows;

/**
 * OtpValidator implementation that always rejects any username + OTP pair.
 */
public class RejectingOtpValidator implements OtpValidator {

  @Override
  public boolean validate(String username, String otp) {
    return false;
  }

  @Override
  public void requestSmsOtp(String username) {}
}
