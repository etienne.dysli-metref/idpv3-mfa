package ch.SWITCH.aai.idp.mfa.flows;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;

import java.util.HashSet;
import java.util.Set;

import javax.security.auth.Subject;

import net.shibboleth.idp.authn.AbstractValidationAction;
import net.shibboleth.idp.authn.AuthenticationFlowDescriptor;
import net.shibboleth.idp.authn.AuthenticationResult;
import net.shibboleth.idp.authn.AuthnEventIds;
import net.shibboleth.idp.authn.context.AuthenticationContext;
import net.shibboleth.idp.authn.principal.PasswordPrincipal;
import net.shibboleth.idp.authn.principal.UsernamePrincipal;
import net.shibboleth.utilities.java.support.logic.ConstraintViolationException;

import org.junit.Test;
import org.mockito.Mockito;
import org.opensaml.messaging.context.BaseContext;
import org.opensaml.profile.context.ProfileRequestContext;

public class ValidateOneTimePasswordAgainstRADIUSTest {

  private final OtpValidator mockOtpValidator = Mockito.mock(OtpValidator.class);
  private final ValidateOneTimePasswordAgainstRADIUS<Object, Object> validateAction = new ValidateOneTimePasswordAgainstRADIUS<Object, Object>(
      mockOtpValidator);

  @Test
  public void shouldExtendAbstractValidationAction() {
    assertThat(validateAction, isA(AbstractValidationAction.class));
  }

  @Test(expected = ConstraintViolationException.class)
  public void shouldThrowWhenGivenNullOtpValidator() {
    new ValidateOneTimePasswordAgainstRADIUS<Object, Object>(null);
  }

  @Test
  public void populateSubject_shouldNotReturnNull() {
    assertThat(validateAction.populateSubject(new Subject()), is(notNullValue()));
  }

  @Test
  public void populateSubject_shouldAddAllUsernamePrincipalsFromInitialSubject() {
    UsernamePrincipal up1 = new UsernamePrincipal("testuser1");
    UsernamePrincipal up2 = new UsernamePrincipal("testuser2");
    Subject initialSubject = new Subject();
    initialSubject.getPrincipals().add(up1);
    initialSubject.getPrincipals().add(up2);
    validateAction.setInitialSubject(initialSubject);
    Subject s = new Subject();
    int sizeBefore = s.getPrincipals().size();
    Subject t = validateAction.populateSubject(s);
    assertThat(t.getPrincipals().size(), equalTo(sizeBefore + 2));
    assertThat(t.getPrincipals(UsernamePrincipal.class), hasItem(sameInstance(up1)));
    assertThat(t.getPrincipals(UsernamePrincipal.class), hasItem(sameInstance(up2)));
  }

  @Test
  public void populateSubject_shouldOnlyAddUsernamePrincipalsFromInitialSubject() {
    PasswordPrincipal pp = new PasswordPrincipal("test");
    Subject initialSubject = new Subject();
    initialSubject.getPrincipals().add(pp);
    validateAction.setInitialSubject(initialSubject);
    Subject s = new Subject();
    int sizeBefore = s.getPrincipals().size();
    Subject t = validateAction.populateSubject(s);
    assertThat(t.getPrincipals().size(), equalTo(sizeBefore));
    assertThat(t.getPrincipals(), not(hasItem(sameInstance(pp))));
  }

  @Test
  public void doExecute_whenValidationSucceeds_shouldCallBuildAuthenticationResult() {
    MockValidateOneTimePasswordAgainstRADIUS<Object, Object> spyva = Mockito
        .spy(new MockValidateOneTimePasswordAgainstRADIUS<Object, Object>(mockOtpValidator));
    Mockito.when(mockOtpValidator.validate(anyString(), anyString())).thenReturn(true);
    ProfileRequestContext<Object, Object> prc = new ProfileRequestContext<Object, Object>();
    AuthenticationContext ac = mockAuthenticationContext();
    spyva.doExecute(prc, ac);
    Mockito.verify(spyva).buildAuthenticationResult(prc, ac);
  }

  @Test
  public void doExecute_shouldSaveInitialAuthenticationSubject() {
    MockValidateOneTimePasswordAgainstRADIUS<Object, Object> mockva = new MockValidateOneTimePasswordAgainstRADIUS<Object, Object>(mockOtpValidator);
    ProfileRequestContext<Object, Object> prc = new ProfileRequestContext<Object, Object>();
    Subject s = mockSubjectWithUsernamePrincipal("testuser");
    mockva.doExecute(prc, mockAuthenticationContext(s));
    assertThat(mockva.getInitialSubject(), is(sameInstance(s)));
  }

  @Test
  public void doExecute_whenInitialAuthenticationResultIsNull_shouldSaveSubjectFromActivePasswordResult() {
    MockValidateOneTimePasswordAgainstRADIUS<Object, Object> mockva = new MockValidateOneTimePasswordAgainstRADIUS<Object, Object>(mockOtpValidator);
    ProfileRequestContext<Object, Object> prc = new ProfileRequestContext<Object, Object>();
    Subject s = mockSubjectWithUsernamePrincipal("testuser");
    AuthenticationContext ac = mockAuthenticationContext(s);
    ac.setInitialAuthenticationResult(null);
    mockva.doExecute(prc, ac);
    assertThat(mockva.getInitialSubject(), is(sameInstance(s)));
  }

  @Test
  public void doExecute_shouldCallOtpValidator() {
    MockValidateOneTimePasswordAgainstRADIUS<Object, Object> mockva = new MockValidateOneTimePasswordAgainstRADIUS<Object, Object>(mockOtpValidator);
    mockva.doExecute(new ProfileRequestContext<Object, Object>(), mockAuthenticationContext());
    Mockito.verify(mockOtpValidator, Mockito.times(1)).validate(anyString(), anyString());
  }

  @Test
  public void doExecute_shouldCallOtpValidatorWithUsernameFromContext() {
    MockValidateOneTimePasswordAgainstRADIUS<Object, Object> mockva = new MockValidateOneTimePasswordAgainstRADIUS<Object, Object>(mockOtpValidator);
    mockva.doExecute(new ProfileRequestContext<Object, Object>(), mockAuthenticationContext());
    Mockito.verify(mockOtpValidator, Mockito.times(1)).validate(eq("testuser"), anyString());
  }

  @Test
  public void doExecute_shouldCallOtpValidatorWithOtpFromContext() {
    MockValidateOneTimePasswordAgainstRADIUS<Object, Object> mockva = new MockValidateOneTimePasswordAgainstRADIUS<Object, Object>(mockOtpValidator);
    mockva.doExecute(new ProfileRequestContext<Object, Object>(), mockAuthenticationContext());
    Mockito.verify(mockOtpValidator, Mockito.times(1)).validate(anyString(), eq("testotp"));
  }

  @Test
  public void doExecute_whenValidationFails_shouldNotCallBuildAuthenticationResult() {
    MockValidateOneTimePasswordAgainstRADIUS<Object, Object> spyva = Mockito
        .spy(new MockValidateOneTimePasswordAgainstRADIUS<Object, Object>(mockOtpValidator));
    Mockito.when(mockOtpValidator.validate(anyString(), anyString())).thenReturn(false);
    spyva.doExecute(new ProfileRequestContext<Object, Object>(), mockAuthenticationContext());
    Mockito.verify(spyva, Mockito.never()).buildAuthenticationResult(any(ProfileRequestContext.class),
        any(AuthenticationContext.class));
  }

  @Test
  public void doExecute_whenValidationFails_shouldCallHandleError() {
    MockValidateOneTimePasswordAgainstRADIUS<Object, Object> spyva = Mockito
        .spy(new MockValidateOneTimePasswordAgainstRADIUS<Object, Object>(mockOtpValidator));
    Mockito.when(mockOtpValidator.validate(anyString(), anyString())).thenReturn(false);
    ProfileRequestContext<Object, Object> prc = new ProfileRequestContext<Object, Object>();
    AuthenticationContext ac = mockAuthenticationContext();
    spyva.doExecute(prc, ac);
    Mockito.verify(spyva, Mockito.times(1)).handleError(prc, ac, AuthnEventIds.INVALID_CREDENTIALS,
        AuthnEventIds.INVALID_CREDENTIALS);
  }

  @Test
  public void doPreExecute_shouldReturnTrue() {
    assertThat(validateAction.doPreExecute(new ProfileRequestContext<Object, Object>(), mockAuthenticationContext()), is(true));
  }

  @Test
  public void doPreExecute_whenAttemptedFlowsIsNull_shouldReturnFalse() {
    ProfileRequestContext<Object, Object> prc = new ProfileRequestContext<Object, Object>();
    AuthenticationContext ac = mockAuthenticationContext();
    ac.setAttemptedFlow(null);
    assertThat(validateAction.doPreExecute(prc, ac), is(false));
  }

  @Test
  public void doPreExecute_whenOtpContextIsNull_shouldReturnFalse() {
    ProfileRequestContext<Object, Object> prc = new ProfileRequestContext<Object, Object>();
    AuthenticationContext ac = mockAuthenticationContext();
    ac.removeSubcontext(OtpContext.class);
    assertThat(validateAction.doPreExecute(prc, ac), is(false));
  }

  @Test
  public void doPreExecute_whenOtpContextIsNull_shouldCallHandleError() {
    MockValidateOneTimePasswordAgainstRADIUS<Object, Object> spyva = Mockito
        .spy(new MockValidateOneTimePasswordAgainstRADIUS<Object, Object>(mockOtpValidator));
    ProfileRequestContext<Object, Object> prc = new ProfileRequestContext<Object, Object>();
    AuthenticationContext ac = mockAuthenticationContext();
    ac.removeSubcontext(OtpContext.class);
    spyva.doPreExecute(prc, ac);
    Mockito.verify(spyva, Mockito.times(1)).handleError(prc, ac, AuthnEventIds.NO_CREDENTIALS,
        AuthnEventIds.NO_CREDENTIALS);
  }

  @Test
  public void doPreExecute_whenOtpIsNull_shouldReturnFalse() {
    ProfileRequestContext<Object, Object> prc = new ProfileRequestContext<Object, Object>();
    AuthenticationContext ac = mockAuthenticationContext();
    ac.getSubcontext(OtpContext.class).setOtp(null);
    assertThat(validateAction.doPreExecute(prc, ac), is(false));
  }

  @Test
  public void doPreExecute_whenOtpIsNull_shouldCallHandleError() {
    MockValidateOneTimePasswordAgainstRADIUS<Object, Object> spyva = Mockito
        .spy(new MockValidateOneTimePasswordAgainstRADIUS<Object, Object>(mockOtpValidator));
    ProfileRequestContext<Object, Object> prc = new ProfileRequestContext<Object, Object>();
    AuthenticationContext ac = mockAuthenticationContext();
    ac.getSubcontext(OtpContext.class).setOtp(null);
    spyva.doPreExecute(prc, ac);
    Mockito.verify(spyva, Mockito.times(1)).handleError(prc, ac, AuthnEventIds.NO_CREDENTIALS,
        AuthnEventIds.NO_CREDENTIALS);
  }

  @Test
  public void extractActiveSubject_shouldReturnInitialAuthnSubject() {
    Subject s = new Subject();
    assertThat(validateAction.extractActiveSubject(mockAuthenticationContext(s)), is(sameInstance(s)));
  }

  @Test
  public void extractActiveSubject_whenInitialAuthenticationResultIsNull_shouldReturnActiveSubject() {
    Subject s = new Subject();
    AuthenticationContext ac = mockAuthenticationContext(s);
    ac.setInitialAuthenticationResult(null);
    assertThat(validateAction.extractActiveSubject(ac), is(sameInstance(s)));
  }

  private AuthenticationContext mockAuthenticationContext() {
    return mockAuthenticationContext(mockSubjectWithUsernamePrincipal("testuser"));
  }

  private AuthenticationContext mockAuthenticationContext(Subject initialAuthenticationSubject) {
    AuthenticationContext ac = new AuthenticationContext();
    AuthenticationResult ar = new AuthenticationResult("authn/Password", initialAuthenticationSubject);
    Set<AuthenticationResult> activeResults = new HashSet<AuthenticationResult>();
    activeResults.add(ar);
    ac.setActiveResults(activeResults);
    ac.setInitialAuthenticationResult(ar);
    ac.setAttemptedFlow(new AuthenticationFlowDescriptor());
    addOtpSubcontext(ac, "testotp");
    return ac;
  }

  private BaseContext addOtpSubcontext(AuthenticationContext ac, String otp) {
    OtpContext oc = new OtpContext();
    oc.setOtp(otp);
    return ac.addSubcontext(oc);
  }

  private Subject mockSubjectWithUsernamePrincipal(String username) {
    Subject s = new Subject();
    s.getPrincipals().add(new UsernamePrincipal(username));
    return s;
  }

  /**
   * Test double to make buildAuthenticationResult() and handleError() visible
   * and remove theirs effects.
   */
  class MockValidateOneTimePasswordAgainstRADIUS<InboundMessageType, OutboundMessageType>
      extends ValidateOneTimePasswordAgainstRADIUS<InboundMessageType, OutboundMessageType> {

    public MockValidateOneTimePasswordAgainstRADIUS(OtpValidator otpValidator) {
      super(otpValidator);
    }

    @Override
    protected void buildAuthenticationResult(
        ProfileRequestContext<InboundMessageType, OutboundMessageType> profileRequestContext,
        AuthenticationContext authenticationContext) {}

    @Override
    protected void handleError(ProfileRequestContext<InboundMessageType, OutboundMessageType> profileRequestContext,
        AuthenticationContext authenticationContext, String message, String eventId) {
      super.handleError(profileRequestContext, authenticationContext, message, eventId);
    }
  }
}
