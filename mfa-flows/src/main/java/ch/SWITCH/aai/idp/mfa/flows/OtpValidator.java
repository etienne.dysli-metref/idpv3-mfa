package ch.SWITCH.aai.idp.mfa.flows;

import javax.annotation.Nonnull;

public interface OtpValidator {
  /**
   * Validate a username + one-time password pair.
   *
   * @param username The user whose one-time password should be validated
   * @param otp The one-time password to validate
   * @return true if the OTP is valid for the given username, false otherwise
   */
  public boolean validate(@Nonnull String username, @Nonnull String otp);

  /**
   * Request an OTP to be sent via SMS to the given user.
   *
   * @param username The user to send the SMS OTP to
   */
  public void requestSmsOtp(@Nonnull String username);
}
