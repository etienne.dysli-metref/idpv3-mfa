package ch.SWITCH.aai.idp.mfa.flows;

import java.io.IOException;

import org.tinyradius.util.RadiusClient;
import org.tinyradius.util.RadiusException;

public class TinyRadiusOtpValidator implements OtpValidator {
  private final RadiusClient radiusClient;

  public TinyRadiusOtpValidator(RadiusClient rc) {
    this.radiusClient = rc;
  }

  @Override
  public boolean validate(String username, String otp) {
    try {
      return radiusClient.authenticate(username, otp);
    } catch (IOException | RadiusException e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  public void requestSmsOtp(String username) {
    try {
      radiusClient.authenticate(username, "sms");
    } catch (IOException | RadiusException e) {
      throw new RuntimeException(e);
    }
  }
}
