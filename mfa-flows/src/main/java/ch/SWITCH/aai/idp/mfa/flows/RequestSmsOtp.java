package ch.SWITCH.aai.idp.mfa.flows;

import javax.annotation.Nonnull;
import javax.security.auth.Subject;

import net.shibboleth.idp.authn.AbstractExtractionAction;
import net.shibboleth.idp.authn.AuthenticationResult;
import net.shibboleth.idp.authn.context.AuthenticationContext;
import net.shibboleth.idp.authn.principal.UsernamePrincipal;
import org.opensaml.profile.action.ActionSupport;
import net.shibboleth.utilities.java.support.logic.Constraint;

import org.opensaml.profile.context.ProfileRequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RequestSmsOtp<InboundMessageType, OutboundMessageType>
    extends AbstractExtractionAction<InboundMessageType, OutboundMessageType> {

  @Nonnull private static final Logger log = LoggerFactory.getLogger(RequestSmsOtp.class);
  @Nonnull private OtpValidator otpValidator;

  public RequestSmsOtp(@Nonnull OtpValidator otpValidator) {
    this.otpValidator = Constraint.isNotNull(otpValidator, "OTP validator cannot be null");
  }

  @Override
  protected void doExecute(ProfileRequestContext<InboundMessageType, OutboundMessageType> profileRequestContext,
      AuthenticationContext authenticationContext) {
    String username = extractFirstUsername(extractActiveSubject(authenticationContext));
    log.debug("{} Requesting SMS OTP for user {}", getLogPrefix(), username);
    otpValidator.requestSmsOtp(username);
    ActionSupport.buildEvent(profileRequestContext, "sms_otp_sent");
  }

  @Nonnull
  public Subject extractActiveSubject(@Nonnull final AuthenticationContext authenticationContext) {
    AuthenticationResult initialResult = authenticationContext.getInitialAuthenticationResult();
    if (initialResult == null) {
      log.debug("{} No initial authentication result found. Looking for active password authentication result in {}",
          getLogPrefix(), authenticationContext.getActiveResults());
      initialResult = authenticationContext.getActiveResults().get("authn/Password");
    }
    log.debug("{} initialResult = {}", getLogPrefix(), initialResult);
    return initialResult.getSubject();
  }

  private String extractFirstUsername(@Nonnull final Subject s) {
    return s.getPrincipals(UsernamePrincipal.class).iterator().next().getName();
  }
}
