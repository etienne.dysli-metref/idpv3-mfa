package ch.SWITCH.aai.idp.mfa.flows;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.opensaml.messaging.context.BaseContext;

public class OtpContext extends BaseContext {
  private String otp;

  @Nullable
  public String getOtp() {
    return otp;
  }

  @Nonnull
  public OtpContext setOtp(@Nullable final String otp) {
    this.otp = otp;
    return this;
  }

}
