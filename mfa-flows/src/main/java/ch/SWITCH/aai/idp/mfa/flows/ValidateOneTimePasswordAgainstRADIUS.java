package ch.SWITCH.aai.idp.mfa.flows;

import javax.annotation.Nonnull;
import javax.security.auth.Subject;

import net.shibboleth.idp.authn.AbstractValidationAction;
import net.shibboleth.idp.authn.AuthenticationResult;
import net.shibboleth.idp.authn.AuthnEventIds;
import net.shibboleth.idp.authn.context.AuthenticationContext;
import net.shibboleth.idp.authn.principal.UsernamePrincipal;
import net.shibboleth.utilities.java.support.logic.Constraint;

import org.opensaml.profile.context.ProfileRequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ValidateOneTimePasswordAgainstRADIUS<InboundMessageType, OutboundMessageType>
    extends AbstractValidationAction<InboundMessageType, OutboundMessageType> {

  @Nonnull private static final Logger log = LoggerFactory.getLogger(ValidateOneTimePasswordAgainstRADIUS.class);
  @Nonnull private Subject initialSubject = new Subject();
  @Nonnull private OtpValidator otpValidator;

  public ValidateOneTimePasswordAgainstRADIUS(@Nonnull OtpValidator otpValidator) {
    this.otpValidator = Constraint.isNotNull(otpValidator, "OTP validator cannot be null");
  }

  @Override
  @Nonnull
  protected Subject populateSubject(@Nonnull final Subject subject) {
    subject.getPrincipals().addAll(initialSubject.getPrincipals(UsernamePrincipal.class));
    return subject;
  }

  @Override
  protected boolean doPreExecute(ProfileRequestContext<InboundMessageType, OutboundMessageType> profileRequestContext,
      AuthenticationContext authenticationContext) {
    if (!super.doPreExecute(profileRequestContext, authenticationContext)) {
      return false;
    }
    OtpContext otpContext = authenticationContext.getSubcontext(OtpContext.class);
    if (otpContext == null) {
        log.info("{} No OtpContext available within authentication context", getLogPrefix());
        handleError(profileRequestContext, authenticationContext, AuthnEventIds.NO_CREDENTIALS, AuthnEventIds.NO_CREDENTIALS);
        return false;
    } else if (otpContext.getOtp() == null) {
        log.info("{} No OTP available within OtpContext", getLogPrefix());
        handleError(profileRequestContext, authenticationContext, AuthnEventIds.NO_CREDENTIALS, AuthnEventIds.NO_CREDENTIALS);
        return false;
    }
    return true;
  }

  @Override
  protected void doExecute(
      @Nonnull final ProfileRequestContext<InboundMessageType, OutboundMessageType> profileRequestContext,
      @Nonnull final AuthenticationContext authenticationContext) {
    initialSubject = extractActiveSubject(authenticationContext);
    if (otpValidator.validate(extractFirstUsername(initialSubject), extractOtp(authenticationContext))) {
      buildAuthenticationResult(profileRequestContext, authenticationContext);
    } else {
      handleError(profileRequestContext, authenticationContext, AuthnEventIds.INVALID_CREDENTIALS,
          AuthnEventIds.INVALID_CREDENTIALS);
    }
  }

  private String extractFirstUsername(@Nonnull final Subject s) {
    return s.getPrincipals(UsernamePrincipal.class).iterator().next().getName();
  }

  private String extractOtp(@Nonnull final AuthenticationContext authenticationContext) {
    return authenticationContext.getSubcontext(OtpContext.class).getOtp();
  }

  @Nonnull
  public Subject extractActiveSubject(@Nonnull final AuthenticationContext authenticationContext) {
    AuthenticationResult initialResult = authenticationContext.getInitialAuthenticationResult();
    if (initialResult == null) {
      log.debug("{} No initial authentication result found. Looking for active password authentication result in {}",
          getLogPrefix(), authenticationContext.getActiveResults());
      initialResult = authenticationContext.getActiveResults().get("authn/Password");
    }
    log.debug("{} initialResult = {}", getLogPrefix(), initialResult);
    return initialResult.getSubject();
  }

  /** Only used for testing */
  Subject getInitialSubject() {
    return initialSubject;
  }

  /** Only used for testing */
  void setInitialSubject(Subject initialSubject) {
    this.initialSubject = initialSubject;
  }

}
