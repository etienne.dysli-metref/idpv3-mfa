package ch.SWITCH.aai.idp.mfa.flows;

import javax.annotation.Nonnull;
import javax.servlet.http.HttpServletRequest;

import net.shibboleth.idp.authn.AbstractExtractionAction;
import net.shibboleth.idp.authn.AuthnEventIds;
import net.shibboleth.idp.authn.context.AuthenticationContext;
import net.shibboleth.utilities.java.support.annotation.constraint.NotEmpty;
import net.shibboleth.utilities.java.support.component.ComponentSupport;
import net.shibboleth.utilities.java.support.logic.Constraint;
import net.shibboleth.utilities.java.support.primitive.StringSupport;

import org.opensaml.profile.action.ActionSupport;
import org.opensaml.profile.context.ProfileRequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ExtractOtpFromFormRequest<InboundMessageType, OutboundMessageType>
    extends AbstractExtractionAction<InboundMessageType, OutboundMessageType> {

  @Nonnull private static final Logger log = LoggerFactory.getLogger(ExtractOtpFromFormRequest.class);
  @Nonnull @NotEmpty private String otpFieldName = "otp";

  public void setOtpFieldName(@Nonnull @NotEmpty final String fieldName) {
    ComponentSupport.ifInitializedThrowUnmodifiabledComponentException(this);
    otpFieldName = Constraint.isNotNull(StringSupport.trimOrNull(fieldName), "OTP field name cannot be null or empty.");
  }

  @Nonnull
  public String getOtpFieldName() {
    return otpFieldName;
  }

  @Override
  protected void doExecute(@Nonnull final ProfileRequestContext<InboundMessageType, OutboundMessageType> profileRequestContext,
      @Nonnull final AuthenticationContext authenticationContext) {
    final OtpContext subContext = authenticationContext.getSubcontext(OtpContext.class, true);
    subContext.setOtp(null);
    final HttpServletRequest request = getHttpServletRequest();
    if (request == null) {
      log.debug("{} Profile action does not contain an HttpServletRequest", getLogPrefix());
      ActionSupport.buildEvent(profileRequestContext, AuthnEventIds.NO_CREDENTIALS);
      return;
    }
    String otpParameter = request.getParameter(otpFieldName);
    if (otpParameter == null || otpParameter.isEmpty()) {
      log.debug("{} No OTP in request", getLogPrefix());
      ActionSupport.buildEvent(profileRequestContext, AuthnEventIds.NO_CREDENTIALS);
      return;
    }
    subContext.setOtp(otpParameter);
  }

}
